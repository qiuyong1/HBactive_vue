

## 支付中心传入参数

> 路径: /#/pay/pay-center

|序号      |参数           |      类型       |     描述  |    默认值     |
|:-------:|:-------------:|:--------------:|:--------:|:------------:|
|1        |     payType   |    enum("H5")  |  支付类型 |       H5     |
|2        |     orderId   |    String      |  订单id  |     null     |
|3        |  sourceFrom   |enum("order","goods")|  支付来源 |   goods |