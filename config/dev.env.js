'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_ROOT: '"https://xhbtest.feiguotech.com/api/"'
  //API_ROOT: '"http://192.168.50.77:8888/"'
})
