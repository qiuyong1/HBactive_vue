import Vue from 'vue'
import Router from 'vue-router'
const NeiGouHui = () => import('@/views/actives/20180408/index') //内购惠活动
const newgoodshlf = () => import('@/views/actives/20180604/index') //新人
const newgoodcoupon = () => import('@/views/actives/20180604/coupon') //新人券
const oldnewEnjoy = ()=>import('@/views/actives/20180410/index') //老的新人优享活动
const YmscJygb = ()=> import('@/views/actives/20180416/index') //20180416一抹色彩活动
//const WxActivity032201 = r => require.ensure([], () => r(require('@/views/actives/20180322/index')), 'WxActivity032201')
const WxActivity032201 = () => import('@/views/actives/20180322/index')//300元大礼包
//const WxActivity032601 = r => require.ensure([], () => r(require('@/views/actives/2018032601/index')), 'WxActivity032601')
const WxActivity032601 = () => import('@/views/actives/2018032601/index')//出游季

const replaceCoupon = () => import('@/views/actives/20180518/index')// 积分兑换优惠券
const invitation = () => import('@/views/actives/20180507/index') // 邀请会员列表
const anniversary = () => import('@/views/actives/20180510/index') //周年庆活动
const huacard = () => import('@/views/actives/20180508/index') //邀请好友激活花卡
const receiveVip = ()=> import('@/views/actives/20180503/index')//领取会员的活动
const ExchangeMembersLogin = ()=> import('@/views/actives/20180502/index') //用兑换码兑换会员
const ExchangeMembers = ()=> import('@/views/actives/20180502/ExchangeMembers')// 兑换花卡会员
const VipActivity = ()=> import('@/views/actives/20180509/index') // 会员活动页面
const getCoupons = ()=> import('@/views/actives/20180511/index')// 领取20元全品类优惠券
const Esteelauder = () => import('@/views/actives/20180515/index') //雅思兰黛活动
const newEnjoy = () => import('@/views/actives/20180516/index')// 优惠券拆分1000元优惠券
const The520 = () => import('@/views/actives/20180520/index') //520活动
const ChildrensDay = () => import('@/views/actives/20180528/index') //儿童节活动
const LinkType = () => import('@/views/iframe/linkType')//iframe
Vue.use(Router)



export default new Router({
  routes: [{
		path: '/neigouhui',
		name: 'NeiGouHui',
		component: NeiGouHui,
		meta: {
			title: '内购惠'
		}
	},
	{
   	  path: '/activity/wxActivity032201',
      name: 'WxActivity032201',
      component: WxActivity032201,
      meta: {
        title: 'WxActivity032201'
      }
    },
  	{
		path: '/oldnewEnjoy',
		name: 'oldnewEnjoy',
		component: oldnewEnjoy,
		meta: {
			title: '新人优享'
		}
	},
	{
		path: '/ymscJygb',
		name: 'YmscJygb',
		component: YmscJygb,
		meta: {
			title: '一抹色彩，为你带来惊艳改变。全场商品199减10'
		}
	},
	{
      path: '/activity/wxActivity032601',
      name: 'WxActivity032601',
      component: WxActivity032601,
      meta: {
        title: '出行游'
      }
    },
	{
		path: '/invitation',
		name: 'invitation',
		component: invitation,
		meta: {
			title: '邀请好友'
		}
	},
	{
		path: '/receiveVip',
		name: 'receiveVip',
		component: receiveVip,
		meta: {
			title: '每天前二十位登录免费领取会员'
		}
	 },
	 {
		path: '/huacard',
		name: 'huacard',
		component: huacard,
		meta: {
			title: '我的花卡'
		}
	},
	{
		path: '/exchangeMembersLogin',
		name: 'ExchangeMembersLogin',
		component: ExchangeMembersLogin,
		meta: {
			title: '兑换码兑换会员登录界面'
		}
	},
	{
		path: '/exchangeMembers',
		name: 'ExchangeMembers',
		component: ExchangeMembers,
		meta: {
			title: '兑换码兑换会员'
		}
	},
	  	{
  		path: '/replaceCoupon',
		name: 'replaceCoupon',
		component: replaceCoupon,
		meta: {
			title: '积分兑换优惠券'
		}
  	},
	{
		path: '/anniversary',
		name: 'anniversary',
		component: anniversary,
		meta: {
			title: '周年庆'
		}
	},
  	{
  		path: '/newEnjoy',
		name: 'newEnjoy',
		component: newEnjoy,
		meta: {
			title: '优惠券拆分1000元'
		}
  	},
  	{
		path: '/VipActivity',
		name: 'VipActivity',
		component: VipActivity,
		meta: {
			title: '会员活动页面'
		}
	},
	{
		path: '/getCoupons',
		name: 'getCoupons',
		component: getCoupons,
		meta: {
			title: '领取优惠券'
		}
	},
   {
	path: '/Esteelauder',
	name: 'Esteelauder',
	component: Esteelauder,
	meta: {
		title: '时光献礼，唯你最美'
		}
	},
	{
	path: '/The520',
	name: 'The520',
	component: The520,
	meta: {
		title: '爱你就“购”了'
		}
  },
  {
    path: '/ChildrensDay',
    name: 'ChildrensDay',
    component: ChildrensDay,
    meta: {
      keepAlive: true,
      title: '真实年龄大质检'
    }
  },
  {
    path: '/newgoodshlf',
    name: 'newgoodshlf',
    component: newgoodshlf,
    meta: {
    	  keepAlive: true,
      title: '分期会员制购物平台'
    }
  },
  {
    path: '/newgoodcoupon',
    name: 'newgoodcoupon',
    component: newgoodcoupon,
    meta: {
      title: '新客专享，优惠多多'
    }
  },
    {
      path: "/link-type", 
      name:"LinkType",
      component:LinkType,
      meta:{
        title:"一抹色彩"
      }
    }
  ]
  ,scrollBehavior (to, from, savedPosition) {//保留滚动的位置
	  if (savedPosition) {
	    return savedPosition
	  } else {
	    return { x: 0, y: 0 }
	  }
	}
})
