import http from '@/util/http'
import Api from '@/util/api'
import Vue from 'vue'
import utils from '@/util/util'
let userId = localStorage.getItem('userId') || '';
const merge = require('webpack-merge')
let state = {
	srcFind:'./static/access/find_active.png'
	,srcClassify:'./static/access/classfly_default.png'
	,srcMe:'./static/access/me_default.png'
};
let getters = {
	
};
let actions = {
	//init
	init ({commit}) {
		if (location.hash.indexOf('/me')>(-1)) {
			 commit('ClickMe');
		}else if (location.hash.indexOf('/find')>(-1)) {
			commit('ClickFind');
		}else{
			commit('ClickClassify');
		}
		
	},
	//点击花伴
	clickfind ({commit}) {
		commit('ClickFind');
	},
	//点击分类
	clickclassify ({commit}) {
		commit('ClickClassify');
	},
	//点击我的
	clickme ({commit}) {
		commit('ClickMe');
	}

};
let mutations = {
	ClickFind (state) {
		state.srcFind = './static/access/find_active.png';
		state.srcClassify = './static/access/classfly_default.png';
		state.srcMe = './static/access/me_default.png';
	},
	ClickClassify (state) {
		state.srcFind = './static/access/find_default.png';
		state.srcClassify = './static/access/classfly_active.png';
		state.srcMe = './static/access/me_default.png';
	},
	ClickMe (state) {
		state.srcFind = './static/access/find_default.png';
		state.srcClassify = './static/access/classfly_default.png';
		state.srcMe = './static/access/me_active.png';
	}
};

export default {
  namespaced:true,
  state,
  getters,
  actions,
  mutations
}
