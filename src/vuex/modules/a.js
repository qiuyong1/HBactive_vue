let state = {
	count: 110
};
let getters;
let actions = {
	increa (context) {
		context.commit('INCREA');
	}
};
let mutations = {
	INCREA (state) {
		state.count++;
	}
};

export default {
  namespaced:true,
  state,
  getters,
  actions,
  mutations
}