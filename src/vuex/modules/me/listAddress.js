import http from '@/util/http'
import Api from '@/util/api'
import Vue from 'vue'
let state = {
	count: 110,
	info:[]
};
let getters;
let actions = {
	init ({commit},params) {
		http.post(Api.listUserAddress,params).then((res) => {
			if(res.code == 200){
				commit('INIT', res);
			}else{
				Vue.prototype.toast(res.message);
			}
		})
		
	}
};
let mutations = {
	INIT (state, data) {
		console.log(data)
	}
};

export default {
  namespaced:true,
  state,
  getters,
  actions,
  mutations
}





