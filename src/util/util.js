function getUserToken(callFuncName, isChek = false) {
	var ua = window.navigator.userAgent.toLowerCase();
	if(isNativeApp()) { //app环境
		var token = sessionStorage.getItem("plus_token") || getQueryString("token");
		if(token && token != ",") {
			if(token.indexOf(",") != -1) return token.split(",")[0];
			return token;
		} else {
			window.location.href = 'huaban://login';
		}
	} else if(isWxPlus()) { //小程序环境
		var token = sessionStorage.getItem("plus_token") || getQueryString("token");
		if(token) {
			return token;
		} else {
			wx.miniProgram.navigateTo({
				url: "/pages/login/login"
			})
		}
	} else {
		//测试。线上不用
		return sessionStorage.getItem("plus_token") || getQueryString("token");
	}
	return null;
}

function isWxWeb() {
	var ua = window.navigator.userAgent.toLowerCase();
	if(ua.match(/MicroMessenger/i) == 'micromessenger') {
		return true;
	} else {
		return false;
	}
}

function login() {
	if(isNativeApp()) { //app环境
		window.location.href = 'huaban://login';
	} else if(isWxPlus()) { //小程序环境
		wx.miniProgram.navigateTo({
			url: "/pages/login/login"
		})
	}
}

function goCredit() {
	if(isNativeApp() && isNewApp()) {
		window.location.href = `huaban://applyCredit`;
		return true;
	} else if(isWxPlus()) {
		wx.miniProgram.navigateTo({
			url: "/msh/package/pages/credit/tcertification/startcertification"
		})
		return true;
	}
	return false;
}

function isNewApp() {
	//TODO
	return true;
}

function goGoods(goodId) {
	if(isNativeApp()) {
		location.href = `huaban://showCommodityDetail?id=${goodId}`;
	} else if(isWxPlus()) {
		wx.miniProgram.navigateTo({
			url: `/pages/goods/goodsInfo/goodsInfo?id=${goodId}`
		})
	} else {
		//TODO 现在不支持浏览器了。这里仅仅测试使用
		window.location.href = "http://feiguo-test.qiuyong.club/goods_main.html?id=" + goodId
	}
}

function goActiveGoods(goodId, activityId) {
	if(isNativeApp()) {
		location.href = `huaban://showCommodityDetail?id=${goodId}&activityId=${activityId}`;
	} else if(isWxPlus()) {
		wx.miniProgram.navigateTo({
			url: `/pages/active/goodsInfo?id=${goodId}&activeId=${activityId}`
		})
	} else {
		//TODO 现在不支持浏览器了。这里仅仅测试使用
		//window.location.href = `http://feiguo-test.qiuyong.club/active_main.html?version=2.3.05&id=${goodId}&activeId=${activityId}`
		window.location.href = `http://weixin.za-huaban.com/goods_main.html?version=2.3.05&id=${goodId}&activeId=${activityId}`
	}
}

function isNativeApp() {
	var ua = window.navigator.userAgent.toLowerCase();
	var regx = /huaban/
	if(regx.test(ua)) {
		return true;
	} else {
		return false;
	}
}

function getCaption(obj){ // 提取huaban后面字符串的方法-->用来判断app版本
    var index=obj.lastIndexOf("\huaban");
    obj=obj.substring(index+7,obj.length);
    return obj;
}

function isNewAppForNum() {
	var appVersion = getCaption(window.navigator.userAgent.toLowerCase())
	if(Number(appVersion) > 2018050504) {
		return true;
	} else {
		return false;
	}
}



function isWxPlus() {
	return(window.__wxjs_environment === 'miniprogram');
}

function getQueryString(name, str) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
	var r = (str || window.location.hash).substr((window.location.hash.indexOf('?')) + 1).match(reg);
	if(r != null) return unescape(r[2]);
	return null;
}

function goHome() {
	if(isNativeApp()) {
		location.href = `huaban://back`;
	} else if(isWxPlus()) {
		wx.miniProgram.switchTab({
			url: "/pages/find/find"
		})
	}
}

function goBack() {
	if(isNativeApp()) {
		location.href = `huaban://back`;
	} else if(isWxPlus()) {
		wx.miniProgram.navigateBack();
	} else {
		return true;
	}
}

function goIndex() {
	if(utils.isNativeApp()) {
		window.location.href = "huaban://continueBrowse";
	} else if(utils.isWxPlus()) {
		wx.miniProgram.switchTab({
			url: "/pages/find/find"
		})
	} else {
		return true;
	}
}

function goOrder(orderId) {
	if(utils.isNativeApp()) {
		window.location.href = `huaban://showOrderDetail?id=${orderId}`;
	} else if(utils.isWxPlus()) {
		wx.miniProgram.navigateTo({
			url: "/pages/order/orderInfo/orderInfo?oid=" + orderId
		})
	} else {
		return true;
	}
}

function object2query(o) {
	var r = [];
	for(var key_ in o) {
		r.push(`${key_}=${o[key_]}`);
	}
	return r.join("&");
}

function getUserInfo() {
	try {
		var value = localStorage.getItem('user_info')
		if(value) {
			return JSON.parse(value);
		} else {
			return null;
		}
	} catch(e) {
		return null;
	}
}

function getUsersessionInfo() {
	try {
		var value = sessionStorage.getItem('user_info')
		if(value) {
			return JSON.parse(value);
		} else {
			return null;
		}
	} catch(e) {
		return null;
	}
}

function splitK(num) {
	var decimal = String(num).split('.')[1] || ''; //小数部分
	var tempArr = [];
	var revNumArr = String(num).split('.')[0].split("").reverse(); //倒序
	for(var i in revNumArr) {
		tempArr.push(revNumArr[i]);
		if((i + 1) % 3 === 0 && i != revNumArr.length - 1) {
			tempArr.push(',');
		}
	}
	var zs = tempArr.reverse().join(''); //整数部分
	return decimal ? zs + '.' + decimal : zs;
}
var format = function(dateObj, fmt) {
	var that = dateObj;
	var o = {
		"M+": that.getMonth() + 1, //月份
		"d+": that.getDate(), //日
		"h+": that.getHours(), //小时
		"m+": that.getMinutes(), //分
		"s+": that.getSeconds(), //秒
		"q+": Math.floor((that.getMonth() + 3) / 3), //季度
		"S": that.getMilliseconds() //毫秒
	};
	if(/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (that.getFullYear() + "").substr(4 - RegExp.$1.length));
	for(var k in o)
		if(new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	return fmt;
}

function goNormGoods(goodId, normId) {
	if(isNativeApp()) {
		location.href = `huaban://showCommodityDetail?id=${goodId}&normId=${normId}`;
	} else if(isWxPlus()) {
		wx.miniProgram.navigateTo({
			url: `/pages/goods/goodsInfo/goodsInfo?id=${goodId}&normId=${normId}`
		})
	} else {
		//TODO 现在不支持浏览器了。这里仅仅测试使用
		//window.location.href = `http://feiguo-test.qiuyong.club/active_main.html?version=2.3.05&id=${goodId}&normId=${normId}`
		//window.location.href = `http://weixin.za-huaban.com/goods_main.html?version=2.3.05&id=${goodId}&normId=${normId}`
		//  window.location.href = `http://feiguo-test.qiuyong.club/oldh5/active_main.html?version=2.3.05&id=${goodId}&normId=${normId}`
		window.location.href = `http://weixin.za-huaban.com/goods_main.html?version=2.3.05&id=${goodId}&normId=${normId}`
	}
}
export default {
	isNewAppForNum,
	getQueryString,
	isWxPlus,
	isNativeApp,
	getUserToken,
	goGoods,
	goCredit,
	goHome,
	login,
	object2query,
	goActiveGoods,
	goBack,
	goIndex,
	goOrder,
	getUserInfo,
	splitK,
	format,
	getUsersessionInfo,
	goNormGoods,
	isWxWeb
}