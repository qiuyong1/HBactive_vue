import axios from 'axios'
import qs from 'qs'
let merge = require('webpack-merge')
let config = process.env;
export default {
	post(url, data = {}, headers = {}, isLogin = true) {
		if (typeof headers == "boolean"){
			isLogin = headers;
		}
		return new Promise((resolve, reject) => {
			axios({
					method: 'post',
					url: config.API_ROOT + url,
					data: data,
					timeout: 10000,
					headers: merge({
//						"Content-Type":"application/json;charset=utf-8",
						"version": config.version,
						"src": config.src,
						"token": sessionStorage.getItem("plus_token"),
//						"channelCode":(sessionStorage.getItem("channelCode") || config.channelCode),
						"screen": 750
					}, headers)
				})
				.then((d) => {
					if(d.status == 200) {
						if(d.data.code == 1011 && isLogin) {
							if(_vue){
								_vue.$vux.loading.hide();
								_vue.$vux.confirm.show({
									title: "花伴商城",
									content: "你还没有登陆,是否登陆？",
									onConfirm: () => {
										_vue.$router.push("/login");
									}
								})
							}
						}else if(d.data.code == 1011) {
							
						} else  {
							resolve(d.data);
						}
					} else {
						reject(d);
					}
				})
				.catch((e) => {
					if(_vue){
						_vue.$vux.loading.hide();
						_vue.toast("网络或服务器错误！");
					}
					reject(e);
				})
		})
	}
}