'use strict'

const productionData = {
	'/newEnjoy': {
		activeId: 8,
		groupIds: [ 30, 26, 31, 27, 29, 28 ]
	},
	'/ymscJygb': {
		activeId: 66
	}
}
const developmentData = {
	'/newEnjoy': {
		activeId: 209,
		groupIds: [ 1, 2, 3, 4, 5, 6 ]
	},
	'/ymscJygb': {
		activeId: 58
	}
}

module.exports = { productionData, developmentData }